<?php

namespace App\Modules\Contracts;

interface UserInterface
{
    public function create($data);

    public function update($id, $data);

    public function delete($id);
}