<?php

namespace App\Modules\Repositories;

use App\Exceptions\ValidationException;
use App\Modules\Contracts\UserInterface;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    private function validate($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $errors = [];
            //TODO add error message handler to helper
            foreach ($validator->errors()->toArray() as $column => $message) {
                $errors[$column] = current($message);
            }
            throw new ValidationException($errors);
        }
    }

    private function hashPassword($password)
    {
        return \Hash::make($password);
    }

    public function create($data)
    {
        $rules = [
            'user_title' => 'required|numeric',
            'first_name' => 'required|alpha|min:3|max:100',
            'last_name' => 'alpha',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|min:10|max:15',
            'dob' => 'date_format:Y-m-d',
            'postal_code' => 'required|min:4|max:15',
            'house_number' => 'required|min:4|max:50',
            'addition' => 'max:50',
        ];
        $data = current($data);
        $this->validate($data, $rules);
        $data['title'] = $data['user_title'];
        $user = $this->user->create($data);
        return success(['user' => $user->getAttributes()], 201);

    }

    public function update($id, $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}