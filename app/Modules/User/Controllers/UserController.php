<?php

namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contracts\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = \App::make(UserInterface::class);
    }

    public function create(Request $request)
    {
        return $this->user->create($request->all());
    }

}