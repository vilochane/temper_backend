<?php

namespace App\Modules\User;

use App\Modules\Contracts\UserInterface;
use App\Modules\Repositories\UserRepository;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{

    private $namespace = __NAMESPACE__ . '\\Controllers';

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'prefix' => 'api',
            'middleware' => []
        ], function () {
            if (!$this->app->routesAreCached()) {
                require __DIR__ . '/routes.php';
            }
        });

    }

}