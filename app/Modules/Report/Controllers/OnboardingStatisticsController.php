<?php

namespace App\Modules\Report\Controllers;


use App\Modules\Report\Repositories\OnboardingStatisticRepository;

class OnboardingStatisticsController
{
    private $onboardingStatistics;

    public function __construct(OnboardingStatisticRepository $onboardingStatistics)
    {
        $this->onboardingStatistics = $onboardingStatistics;
    }

    public function onboardingStatistics()
    {
        return $this->onboardingStatistics->onboardingStatisticsChartData();
    }
}