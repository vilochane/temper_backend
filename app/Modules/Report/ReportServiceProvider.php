<?php

namespace App\Modules\Report;

use App\Modules\Report\Contracts\OnboardingStatisticInterface;
use App\Modules\Report\Repositories\OnboardingStatisticRepository;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ReportServiceProvider extends ServiceProvider
{
    private $namespace = __NAMESPACE__ . '\\Controllers';

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OnboardingStatisticInterface::class, OnboardingStatisticRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'prefix' => 'api',
            'middleware' => []
        ], function () {
            if (!$this->app->routesAreCached()) {
                require __DIR__ . '/routes.php';
            }
        });

    }

}