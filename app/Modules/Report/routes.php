<?php

Route::group(['prefix' => 'report'], function () {
    Route::get('onboarding-statistics', 'OnboardingStatisticsController@onboardingStatistics');
});