<?php

namespace App\Modules\Report\Models;

use Illuminate\Database\Eloquent\Model;

class OnboardingStatistic extends Model
{
    protected $fillable = [
        'user_id',
        'created_at',
        'onboarding_percentage',
        'count_applications',
        'count_accepted_applications'
    ];
}