<?php

namespace App\Modules\Report\Models;

use Illuminate\Database\Eloquent\Model;

class OnboardingStatisticsView extends Model
{
    protected $table = 'onboarding_statistics_view';

    public function weekly()
    {
        $builder = $this;
        $result = $builder->select(
            'week_no',
            'step',
            \DB::raw('date(created_at) as "date_created"'),
            \DB::raw('((((select count(user_id) from onboarding_statistics_view ) - count(user_id))/ (select count(user_id) from onboarding_statistics_view )) * 100) as users'),
            'onboarding_percentage'
        )
            ->groupBy('week_no')
            ->groupBy('step')
            ->orderBy('week_no')
            ->orderBy('step')->get();
        return $result;
    }
}