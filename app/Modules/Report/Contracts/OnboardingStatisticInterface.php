<?php

namespace App\Modules\Report\Contracts;

interface OnboardingStatisticInterface
{
    public function onboardingStatisticsChartData();
}