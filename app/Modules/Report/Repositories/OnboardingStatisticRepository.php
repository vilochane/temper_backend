<?php

namespace App\Modules\Report\Repositories;

use App\Modules\Report\Contracts\OnboardingStatisticInterface;
use App\Modules\Report\Exceptions\OnboardStatisticsNotFound;
use App\Modules\Report\Models\OnboardingStatisticsView;

class OnboardingStatisticRepository implements OnboardingStatisticInterface
{
    private $onboardingStatistic;

    public function __construct(OnboardingStatisticsView $onboardingStatistic)
    {
        $this->onboardingStatistic = $onboardingStatistic;
    }

    public function onboardingStatisticsChartData()
    {
        $statistics = [];
        $dataSource = $this->onboardingStatistic->weekly();
        if (count($dataSource)) {
            foreach ($dataSource as $result) {
                $data = ['x' => $result['step'], 'y' => (int)$result['users']];
                $index = (int)$result['week_no'];
                $statistics[$index]['name'] = 'week-' . $result['week_no'];
                $statistics[$index]['data'][$result['step']] = $data;
            }
            $statistics = $this->addStartingPoints($statistics);
            return success(['onboard_statistics' => ['weeks' => array_values($statistics)]]);
        }
        throw new OnboardStatisticsNotFound;
    }

    private function addStartingPoints($statistics)
    {
        foreach ($statistics as $index => $statistic) {
            $data = ['x' => 0, 'y' => 100];
            if (!isset($statistics[$index]['data'][0])) {
                $statistics[$index]['data'][0] = $data;
            }
            sort($statistics[$index]['data']);
        }
        return $statistics;
    }
}