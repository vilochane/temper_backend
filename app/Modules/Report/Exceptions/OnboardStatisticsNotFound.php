<?php

namespace App\Modules\Report\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class OnboardStatisticsNotFound extends HttpException
{
    protected $code = 'REP001';

    public function __construct($message = 'No on boarding statistics found.')
    {
        parent::__construct(404, $message);
    }
}