<?php

namespace App\Http\Controllers\Auth\Exceptions;

use Illuminate\Validation\UnauthorizedException as Unauthorized;

class UnauthorizedException extends Unauthorized
{
    public function __construct($message = 'Authentication required.', $code = 401)
    {
        parent::__construct($message, $code);
    }
}