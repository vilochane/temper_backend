<?php

namespace App\Http\Response;

class Error implements ResponseInterface
{
    private $status = "error";
    private $statusCode;
    private $message;
    private $errorMessages;

    /**
     * Error json response
     * @param  integer $statusCode Standed HTTP status code
     * @param  Array|array $errorMessages Custom error messages
     */
    public function __construct(array $errorMessages = [], $statusCode = 400)
    {
        $this->statusCode = $statusCode;
        $this->errorMessages = $errorMessages;
        return $this;
    }

    public function send()
    {
        return (new JsonMessageBuilder())
            ->error()
            ->errors($this->errorMessages)
            ->code($this->statusCode)
            ->send();
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getMessages()
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getJson()
    {
        return json_encode($this->data);
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }


}
