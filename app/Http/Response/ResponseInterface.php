<?php

namespace App\Http\Response;

interface ResponseInterface
{
    public function send();

    public function getStatus();

    public function getStatusCode();

    public function setStatusCode($statusCode);

    public function getMessages();

    public function getJson();

    public function getData();
}
