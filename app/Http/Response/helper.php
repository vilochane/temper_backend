<?php

use App\Http\Response\Error;
use App\Http\Response\Success;

/**
 * Error json response
 * @param  Array|array $errorMessages Custom error messages
 * @param  integer $statusCode Stranded HTTP status code
 * @return \Illuminate\Foundation\Application|mixed
 */
function error(array $errorMessages = [], $statusCode = 400)
{
    return new Error($errorMessages, $statusCode);
}

/**
 * Success Json response
 * @param  array $data response content
 * @param  String $message custom success massage if needed
 * @param int $code
 * @return Success
 */
function success($data = [], $code = 200, $message = null)
{
    return new Success($data, $message, $code);
}
