<?php

namespace App\Http\Middleware;

use App\Http\Response\Success;
use Closure;

class ContentType
{

    public function __construct()
    {

    }

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $content = $response->original;
        if ($content instanceof Success) {
            $response->header('Content-Type', 'application/json');
            $response->setStatusCode($content->getStatusCode());
        }
        return $response;
    }
}
