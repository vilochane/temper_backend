<?php

namespace App\Http\Middleware;

use Closure;

class AccessControlOrigin
{

    public function __construct()
    {

    }

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('Access-Control-Allow-Origin', '*');
        $response->header('Access-Control-Allow-Headers',
            'Origin, x-Access-Token, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Origin');
        $response->header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
}
