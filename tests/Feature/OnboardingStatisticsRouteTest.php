<?php

namespace Tests\Feature;

use Tests\TestCase;

class OnboardingStatisticsRouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRouteGetOnboardingStatistics()
    {
        $url = '/api/report/onboarding-statistics';
        $response = $this->get($url);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
