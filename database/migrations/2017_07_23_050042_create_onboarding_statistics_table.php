<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnboardingStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = 'onboarding_statistics';
        Schema::create($tableName, function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->date('created_at');
            $table->mediumInteger('onboarding_percentage');
            $table->mediumInteger('count_applications');
            $table->mediumInteger('count_accepted_applications');
        });
        DB::statement("ALTER TABLE `$tableName` comment 'On boarding statistics talbe for reporting'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('onboarding_statistics');
    }
}
