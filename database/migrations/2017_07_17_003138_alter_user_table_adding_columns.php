<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTableAddingColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('name');
            $table->string('password')->nullable()->change();

            $table->tinyInteger('title')->after('id');
            $table->string('first_name', 100)->after('title');
            $table->string('last_name', 100)->nullable()->after('first_name');
            $table->string('phone', 20)->after('last_name');
            $table->date('dob')->nullable()->after('phone');
            $table->string('postal_code', 20)->after('dob');
            $table->string('house_number', 50)->after('postal_code');
            $table->string('addition', 50)->after('house_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('name', 50)->after('id');

            $table->dropColumn(['title', 'first_name', 'last_name', 'phone', 'dob', 'postal_code', 'house_number', 'addition']);
        });
    }
}
