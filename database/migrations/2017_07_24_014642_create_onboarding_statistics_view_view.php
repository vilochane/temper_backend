<?php

use Illuminate\Database\Migrations\Migration;

class CreateOnboardingStatisticsViewView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            create or replace view onboarding_statistics_view as
            select `onboarding_statistics`.`user_id` AS `user_id`,`onboarding_statistics`.`created_at` AS `created_at`,
            week(`onboarding_statistics`.`created_at`,0) AS `week_no`,
            `onboarding_statistics`.`onboarding_percentage` AS `onboarding_percentage`,
            (case when (`onboarding_statistics`.`onboarding_percentage` = 0) then 1 
            when (`onboarding_statistics`.`onboarding_percentage` = 20) then 2 
            when (`onboarding_statistics`.`onboarding_percentage` = 40) then 3 
            when (`onboarding_statistics`.`onboarding_percentage` = 50) then 4 
            when (`onboarding_statistics`.`onboarding_percentage` = 70) then 5 
            when (`onboarding_statistics`.`onboarding_percentage` = 75) then 5 
            when (`onboarding_statistics`.`onboarding_percentage` = 90) then 6 
            when (`onboarding_statistics`.`onboarding_percentage` = 99) then 7 
            when (`onboarding_statistics`.`onboarding_percentage` = 100) then 8 end) AS `step` 
            from `onboarding_statistics` 
            where ((`onboarding_statistics`.`onboarding_percentage` = 0) 
            or (`onboarding_statistics`.`onboarding_percentage` = 20) 
            or (`onboarding_statistics`.`onboarding_percentage` = 40) 
            or (`onboarding_statistics`.`onboarding_percentage` = 50) 
            or (`onboarding_statistics`.`onboarding_percentage` = 70) 
            or (`onboarding_statistics`.`onboarding_percentage` = 75) 
            or (`onboarding_statistics`.`onboarding_percentage` = 90) 
            or (`onboarding_statistics`.`onboarding_percentage` = 99) 
            or (`onboarding_statistics`.`onboarding_percentage` = 100))
SQL;

        \DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('drop view if exists onboarding_statistics_view');
    }
}
